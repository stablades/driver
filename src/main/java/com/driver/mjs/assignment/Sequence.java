package com.driver.mjs.assignment;

import java.util.List;
import java.util.Objects;

/**
 * Representation of a sequence with overlap heuristic and index if applicable.
 * Sequence name is not currently used but could be utilized in the future.
 */
public class Sequence {
    private String name;
    private List<Nucleotide> sequence;

    public Sequence(String name, List<Nucleotide> sequence) {
        this.name = name;
        this.sequence = sequence;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Sequence)) return false;

        Sequence objSequence = (Sequence) obj;
        return Objects.equals(this.sequence, objSequence.sequence);
    }

    @Override
    public String toString() {
        String s = "";
        for (Nucleotide nucleotide: this.sequence) {
            s += nucleotide.name();
        }
        return s;
    }

    private int overlapIndexWith(Sequence otherSequence) {
        int overlapPoint = (int)(otherSequence.sequence.size()/2) + 1;
        int index = -1;

        for (int i = overlapPoint ; i <= otherSequence.sequence.size(); i++) {
            if (this.sequence.size() < i) break;
            if (otherSequence.sequence.subList(0, i).equals(this.sequence.subList(this.sequence.size() - i, this.sequence.size()))) {
                index = i;
                break;
            }
        }
        return index;
    }

    public Sequence getRemainderAfterOverlapWith(Sequence otherSequence) {
        return new Sequence(this.name + "-" + otherSequence.name, otherSequence.sequence.subList(overlapIndexWith(otherSequence), otherSequence.sequence.size()));
    }

    public boolean overlaps(Sequence otherSequence) {
        return overlapIndexWith(otherSequence) != -1;
    }

    public Sequence appendSequence(Sequence secondSequence) {
        this.sequence.addAll(secondSequence.sequence);
        return this;
    }
}