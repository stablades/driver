package com.driver.mjs.assignment;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Cleans input data by trimming whitespace, uppercasing letters, and combining
 * multi-line sequences.
 */
public class FastaCleaner {
    private static final String nameExpression = ">.*";
    private static Pattern namePattern = Pattern.compile(nameExpression);

    private static boolean isName(String string) {
        return namePattern.matcher(string).find();
    }

    public static List<String> clean(List<String> list) {
        List<String> cleanList = new ArrayList<String>();
        int i = 0;

        while (i < list.size()) {
            if (isName(list.get(i))) {
                cleanList.add(list.get(i).trim());
                i++;
            } else {
                i = combineNucleotides(cleanList, list, i);
            }
        }

        return cleanList;
    }

    private static int combineNucleotides(List<String> cleanList, List<String> list, int index) {
        String nucleotides = "";

        while (!(list.size() == index || isName(list.get(index)))) {
            nucleotides += list.get(index).toUpperCase();
            index++;
        }

        cleanList.add(nucleotides);
        return index;
    }
}
