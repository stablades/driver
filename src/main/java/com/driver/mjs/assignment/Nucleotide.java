package com.driver.mjs.assignment;

/**
 * Enum to store Nucleotide data type.
 */
public enum Nucleotide {
    A, C, G, T;
}
