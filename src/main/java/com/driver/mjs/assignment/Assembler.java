package com.driver.mjs.assignment;

import java.util.Iterator;
import java.util.List;
import org.jgrapht.experimental.dag.DirectedAcyclicGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 * Takes a list of sequences, constructs a directed acyclic graph, and
 * performs a topological sort to construct the unique overlapping sequence.
 */
public class Assembler {

    private DirectedAcyclicGraph<Sequence, DefaultEdge> dag;

    public Assembler(DirectedAcyclicGraph<Sequence, DefaultEdge> dag) {
        this.dag = dag;
    }

    public Sequence assemble(List<Sequence> sequences) {
        for (Sequence sequence: sequences) {
            this.dag.addVertex(sequence);
        }

        for (Sequence sequence1: sequences) {
            for (Sequence sequence2: sequences) {
                if (sequence1 != sequence2) {
                    if (sequence1.overlaps(sequence2)) {
                        this.dag.addEdge(sequence1, sequence2);
                    }
                }
            }
        }

        Iterator<Sequence> orderIterator = this.dag.iterator();

        Sequence firstSequence = orderIterator.next();
        Sequence masterSequence = firstSequence;

        while (orderIterator.hasNext()) {
            Sequence secondSequence = orderIterator.next();
            masterSequence.appendSequence(firstSequence.getRemainderAfterOverlapWith(secondSequence));
            firstSequence = secondSequence;
        }

        return masterSequence;
    }
}
