package com.driver.mjs.assignment;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Validates that the input data has between 1-50 sequences, no more than 1000
 * characters per sequence, and conforms to the Fasta format once cleaned.
 */
public class FastaValidator {
    private static final int MAX_SEQUENCES = 50;
    private static final int MAX_CHARS_PER_SEQUENCE = 1000;

    private static final String nameExpression = ">.*";
    private static final String sequenceExpression = String.format("^[ACGT]{1,%s}$", MAX_CHARS_PER_SEQUENCE);
    private static Pattern namePattern = Pattern.compile(nameExpression);
    private static Pattern sequencePattern = Pattern.compile(sequenceExpression);

    public static boolean checkValidity(List<String> lines) throws InvalidFileFormatException {
        if (lines.size() % 2 == 1) {
            throw new InvalidFileFormatException("Must alternate between name and sequence");
        } else if (lines.size() > MAX_SEQUENCES * 2) {
            throw new InvalidFileFormatException("Too many sequences! Maximum is " + MAX_SEQUENCES);
        } else if (lines.size() == 0) {
            throw new InvalidFileFormatException("Cannot supply an empty file");
        }


        for (int i = 0; i < lines.size(); i++) {
            Pattern pattern = i % 2 == 0 ? namePattern : sequencePattern;
            if (!patternIsValid(pattern, lines.get(i))) {
                throw new InvalidFileFormatException("File must conform to Fasta format");
            }
        }

        return true;
    }

    private static boolean patternIsValid(Pattern pattern, String line) {
       return pattern.matcher(line).find();
    }
}