package com.driver.mjs.assignment;

import org.jgrapht.experimental.dag.DirectedAcyclicGraph;
import org.jgrapht.graph.DefaultEdge;

import java.io.IOException;
import java.util.List;

/**
 * Wrapper class that reads a file, cleans, validates, and parses input, and finds unique sequence.
 */
public class Reconstructor {

    public static Sequence reconstructSequenceFromFile(String filePath) throws IOException, InvalidFileFormatException {
        List<String> list = IOHandler.getLinesFromFile(filePath);
        list = FastaCleaner.clean(list);
        FastaValidator.checkValidity(list);

        DirectedAcyclicGraph<Sequence, DefaultEdge> dag = new DirectedAcyclicGraph<Sequence, DefaultEdge>(DefaultEdge.class);

        Assembler assembler = new Assembler(dag);
        return assembler.assemble(FastaParser.getSequencesFromList(list));
    }
}