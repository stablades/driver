package com.driver.mjs.assignment;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses input data into a list of sequences.
 */
public class FastaParser {

    public static List<Sequence> getSequencesFromList(List<String> list) {
        List<Sequence> sequences = new ArrayList<Sequence>();

        for (int i = 0; i < list.size(); i += 2) {
            sequences.add(new Sequence(getNameFromString(list.get(i)), getNucleotidesFromString(list.get(i + 1))));
        }

        return sequences;
    }

    private static List<Nucleotide> getNucleotidesFromString(String nucleotide) {
        List<Nucleotide> sequence = new ArrayList<Nucleotide>();

        for(char c : nucleotide.toCharArray()) {
            sequence.add(Nucleotide.valueOf(String.valueOf(c)));
        }

        return sequence;
    }

    private static String getNameFromString(String name) {
        Matcher nameMatcher = Pattern.compile(">(.*)").matcher(name);
        nameMatcher.matches();
        return nameMatcher.group(1);
    }
}