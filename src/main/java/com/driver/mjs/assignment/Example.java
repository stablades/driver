package com.driver.mjs.assignment;

import java.io.IOException;

/**
 * Example showing intended usage of reconstructor.
 */
public class Example {
    public static void main(String [] args) {
        Reconstructor reconstructor = new Reconstructor();
        try {
            System.out.println(reconstructor.reconstructSequenceFromFile("./src/main/java/com/driver/mjs/assignment/input"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFileFormatException e) {
            e.printStackTrace();
        }
    }
}
