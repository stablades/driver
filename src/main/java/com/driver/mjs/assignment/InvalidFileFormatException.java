package com.driver.mjs.assignment;

/**
 * Custom exception to handle Fasta file validation.
 */
public class InvalidFileFormatException extends Exception {
    public InvalidFileFormatException(String message) {
        super(message);
    }
}
