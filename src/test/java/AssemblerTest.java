import com.driver.mjs.assignment.Assembler;
import com.driver.mjs.assignment.Nucleotide;
import com.driver.mjs.assignment.Sequence;
import org.jgrapht.experimental.dag.DirectedAcyclicGraph;
import org.jgrapht.graph.DefaultEdge;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class AssemblerTest {

    @Test
    public void shouldAssembleSequenceFromList() {
        Sequence sequence1 = SequenceHelper.asList(Nucleotide.A, Nucleotide.C, Nucleotide.G);
        Sequence sequence2 = SequenceHelper.asList(Nucleotide.C, Nucleotide.G, Nucleotide.T);
        List<Sequence> input = Arrays.asList(sequence1, sequence2);

        DirectedAcyclicGraph<Sequence, DefaultEdge> dag = mock(DirectedAcyclicGraph.class);
        when(dag.iterator()).thenReturn(Arrays.asList(sequence1, sequence2).iterator());

        Assembler assembler = new Assembler(dag);

        Sequence expected = SequenceHelper.asList(Nucleotide.A, Nucleotide.C, Nucleotide.G, Nucleotide.T);
        assertEquals(assembler.assemble(input), expected);

        verify(dag, times(2)).addVertex(any(Sequence.class));
        verify(dag, times(1)).addEdge(any(Sequence.class), any(Sequence.class));
        verify(dag, times(1)).iterator();
    }
}
