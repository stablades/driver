import com.driver.mjs.assignment.Nucleotide;
import com.driver.mjs.assignment.Sequence;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matthew on 11/7/16.
 */
public class SequenceHelper {
    public static Sequence asList(Nucleotide... nucleotides) {
        List<Nucleotide> sequence = new ArrayList<Nucleotide>();
        for (Nucleotide nucleotide : nucleotides) {
            sequence.add(nucleotide);
        }
        return new Sequence("test", sequence);
    }
}
