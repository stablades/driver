import com.driver.mjs.assignment.FastaValidator;
import com.driver.mjs.assignment.InvalidFileFormatException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by matthew on 11/7/16.
 */
public class FastaValidatorTest {

    private List<String> testList() {
        return Arrays.asList(">Frag_56", "AT", ">Frag_57", "CC");
    }

    private List<String> listEndingInName() {
        return testList().subList(0, testList().size() - 1);
    }

    @Test
    public void shouldThrowExceptionIfInvalidInput() {
        try {
            FastaValidator.checkValidity(listEndingInName());
            fail("Exception not thrown!");
        } catch (InvalidFileFormatException e) {}
    }

    @Test
    public void shouldThrowExceptionIfEmptyInput() {
        try {
            FastaValidator.checkValidity(new ArrayList<String>());
            fail("Exception not thrown!");
        } catch (InvalidFileFormatException e) {}
    }

    @Test
    public void shouldReturnTrueIfValidInput() throws InvalidFileFormatException {
        assertTrue(FastaValidator.checkValidity(testList()));
    }
}