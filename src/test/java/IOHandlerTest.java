import com.driver.mjs.assignment.IOHandler;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by matthew on 11/7/16.
 */
public class IOHandlerTest {
    @Test
    public void shouldReadLinesFromFile() throws IOException {
        List<String> input = IOHandler.getLinesFromFile("./src/test/java/fasta");
        List<String> expected = Arrays.asList(">Frag_56", "ATTAGACCTG", ">Frag_57", "CCTGCCGGAA", ">Frag_58", "AGACCTGCCG", ">Frag_59", "GCCGGAATAC");

        assertEquals(input, expected);
    }
}