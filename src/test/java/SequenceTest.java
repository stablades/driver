import com.driver.mjs.assignment.Nucleotide;
import com.driver.mjs.assignment.Sequence;
import org.junit.Test;

import static org.junit.Assert.*;

public class SequenceTest {
    @Test
    public void shouldNotBeEqualToDifferentClassObject() {
        Sequence sequence = SequenceHelper.asList(Nucleotide.A, Nucleotide.C);
        String notASequence = "not a sequence";

        assertNotEquals(sequence, notASequence);
    }

    @Test
    public void shouldBeEqualToObjectWithDifferentSequence() {
        Sequence sequence1 = SequenceHelper.asList(Nucleotide.A, Nucleotide.C);
        Sequence sequence2 = SequenceHelper.asList(Nucleotide.C, Nucleotide.A);

        assertNotEquals(sequence1, sequence2);
    }

    @Test
    public void shouldBeEqualToSameObject() {
        Sequence sequence = SequenceHelper.asList(Nucleotide.A, Nucleotide.C);

        assertEquals(sequence, sequence);
    }

    @Test
    public void shouldBeEqualToObjectWithSameSequence() {
        Sequence sequence1 = SequenceHelper.asList(Nucleotide.A, Nucleotide.C);
        Sequence sequence2 = SequenceHelper.asList(Nucleotide.A, Nucleotide.C);

        assertEquals(sequence1, sequence2);
    }

    @Test
    public void shouldReturnSequenceAsString() {
        Sequence sequence = SequenceHelper.asList(Nucleotide.A, Nucleotide.C);

        assertEquals(sequence.toString(), "AC");
    }

    @Test
      public void shouldAppendTwoSequences() {
        Sequence sequence1 = SequenceHelper.asList(Nucleotide.A, Nucleotide.C);
        Sequence sequence2 = SequenceHelper.asList(Nucleotide.A, Nucleotide.C);

        assertEquals(sequence1.appendSequence(sequence2), SequenceHelper.asList(Nucleotide.A, Nucleotide.C, Nucleotide.A, Nucleotide.C));
    }

    @Test
    public void shouldReturnTrueWithOverlappingSequences() {
        Sequence sequence1 = SequenceHelper.asList(Nucleotide.A, Nucleotide.C, Nucleotide.G);
        Sequence sequence2 = SequenceHelper.asList(Nucleotide.C, Nucleotide.G);

        assertTrue(sequence1.overlaps(sequence2));
    }

    @Test
    public void shouldReturnFalseWithNonOverlappingSequences() {
        Sequence sequence1 = SequenceHelper.asList(Nucleotide.A, Nucleotide.C, Nucleotide.G);
        Sequence sequence2 = SequenceHelper.asList(Nucleotide.T, Nucleotide.G);

        assertFalse(sequence1.overlaps(sequence2));
    }

    @Test
    public void shouldReturnFalseWithAlmostOverlappingSequences() {
        Sequence sequence1 = SequenceHelper.asList(Nucleotide.A, Nucleotide.C);
        Sequence sequence2 = SequenceHelper.asList(Nucleotide.C, Nucleotide.G);

        assertFalse(sequence1.overlaps(sequence2));
    }

    @Test
    public void shouldReturnTrueWithEntirelyOverlappingSequences() {
        Sequence sequence1 = SequenceHelper.asList(Nucleotide.A, Nucleotide.C, Nucleotide.G);
        Sequence sequence2 = SequenceHelper.asList(Nucleotide.C, Nucleotide.G);

        assertTrue(sequence1.overlaps(sequence2));
    }

    @Test
    public void shouldGetRemainderAfterOverlapWithSequence() {
        Sequence sequence1 = SequenceHelper.asList(Nucleotide.A, Nucleotide.C, Nucleotide.G);
        Sequence sequence2 = SequenceHelper.asList(Nucleotide.C, Nucleotide.G, Nucleotide.T);

        assertEquals(sequence1.getRemainderAfterOverlapWith(sequence2), SequenceHelper.asList(Nucleotide.T));
    }
}