import com.driver.mjs.assignment.FastaParser;
import com.driver.mjs.assignment.Nucleotide;
import com.driver.mjs.assignment.Sequence;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by matthew on 11/7/16.
 */
public class FastaParserTest {

    @Test
    public void shouldGetListOfSequences() {
        List<String> inputList = Arrays.asList(">Frag_56", "AT", ">Frag_57", "CC");
        Assert.assertEquals(FastaParser.getSequencesFromList(inputList),
                Arrays.asList(new Sequence("Frag_56", Arrays.asList(Nucleotide.A, Nucleotide.T)),
                        new Sequence("Frag_57", Arrays.asList(Nucleotide.C, Nucleotide.C))));
    }

}