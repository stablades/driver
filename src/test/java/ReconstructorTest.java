import com.driver.mjs.assignment.InvalidFileFormatException;
import com.driver.mjs.assignment.Nucleotide;
import com.driver.mjs.assignment.Reconstructor;
import com.driver.mjs.assignment.Sequence;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created by matthew on 11/7/16.
 */
public class ReconstructorTest {
    @Test
    public void shouldReconstructSequence() throws IOException, InvalidFileFormatException {
        Sequence actual = Reconstructor.reconstructSequenceFromFile("./src/test/java/fasta");
        Sequence expected = SequenceHelper.asList(Nucleotide.A, Nucleotide.T, Nucleotide.T,
                                                   Nucleotide.A, Nucleotide.G, Nucleotide.A,
                                                   Nucleotide.C, Nucleotide.C, Nucleotide.T,
                                                   Nucleotide.G, Nucleotide.C, Nucleotide.C,
                                                   Nucleotide.G, Nucleotide.G, Nucleotide.A,
                                                   Nucleotide.A, Nucleotide.T, Nucleotide.A,
                                                   Nucleotide.C);

        assertEquals(actual, expected);
    }

}