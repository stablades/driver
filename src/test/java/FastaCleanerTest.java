import com.driver.mjs.assignment.FastaCleaner;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by matthew on 11/7/16.
 */
public class FastaCleanerTest {

    @Test
    public void shouldTrimExtraWhitespace() {
        List<String> cleanList = Arrays.asList(">Frag_56", "ATTAGACCTG", ">Frag_57", "CCTGCCGGAA");
        List<String> extraWhiteSpaceList = Arrays.asList(">Frag_56  ", "ATTAGACCTG", "    >Frag_57", "CCTGCCGGAA");
        Assert.assertEquals(FastaCleaner.clean(extraWhiteSpaceList), cleanList);
    }

    @Test
    public void shouldUppercaseSequence() {
        List<String> cleanList = Arrays.asList(">Frag_56", "ATTAGACCTG", ">Frag_57", "CCTGCCGGAA");
        List<String> lowercaseList = Arrays.asList(">Frag_56  ", "attagacctg", "    >Frag_57", "CCTGCCGGAA");
        assertEquals(FastaCleaner.clean(lowercaseList), cleanList);
    }

    @Test
    public void shouldCombineMultipleLineSequences() {
        List<String> cleanList = Arrays.asList(">Frag_56", "ATTAGACCTGATTAGACCTG", ">Frag_57", "CCTGCCGGAA");
        List<String> multipleLineList = Arrays.asList(">Frag_56", "ATTAGACCTG", "ATTAGACCTG", ">Frag_57", "CCTGCCGGAA");
        assertEquals(FastaCleaner.clean(multipleLineList), cleanList);
    }
}