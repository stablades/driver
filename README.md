##### Required Dev Tools:
* Homebrew ```/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"```
* Maven ```brew install maven```

##### Actions
* From project directory:
    * ```mvn package``` 
*Validate, compile, test, and package*
    * ```java -cp target/matthew-sloane-assignment-1.0-SNAPSHOT-jar-with-dependencies.jar com.driver.mjs.assignment.Example``` *Run reconstructor with given input sequence and log result to console. In practice, the reconstructor itself would be consumed, not the example.*

##### Assumptions:
* **Sequence 1 overlaps Sequence 2 if and only if it overlaps by half of Sequence 2's length**
    * *For example, ABCDEFGH overlaps FGHIJ, but ABCDE does not overlap CDEFGHI*
    * A different overlap heuristic could easily supplant the existing method if there is another interpretation of overlapping.

* **One sequence can overlap multiple sequences in the input list**
    * *For example, given [ABCDE, CDEFG, CDEGF, and GFCDE], the first element overlaps both the second and third element. Choosing to overlap the second element will lead to a dead end.*

* **Two sequences have at most one valid overlap.**
    * *For example, there is no situation with AAA and AAA, with valid overlaps of AAA and AAAA.*
    * Without this assumption, an additional dimension of complexity is added and it is no longer possible to use a 2D dependency graph.

##### Approach:
This problem can be divided into two subproblems: evaluating whether two sequences overlap, and how to order overlapping sequences.

* **Evaluating whether Sequence 1 overlaps Sequence 2 of length n:**
    * This is done naively by taking all ~1/2n subsequences of Sequence 2 that fit the overlap heuristic, and comparing them to the trailing end of Sequence 1 (O(n) for equality check).  This yields an overall complexity of ~1/2n * n = O(n^2).

* **Finding the order of m overlapping sequences:**
    * By the project formulation, we know that there exists a unique overlapping sequence. To start, we can create a directed dependency graph from the different overlapping pairs. For example, if A overlaps B, then we have a directed edge A -> B. The overlap property cannot be symmetric as this would mean that the reverse sequence order would also be valid and thus not unique.  We also know that this graph is acyclic by a similar rationale, since otherwise there would be m valid orderings (if we had A->B->C->A, then ABC, BCA, and CAB would all be valid).  Since this graph is both directed and acyclic, we can find the Hamiltonian path using a standard topological sort and retrieve the vertex path that identifies the unique sequence ordering.

    * I have utilized the graph library JGraphT to aid with the graph data structure. Given m sequences, constructing the dependency graph is of O(m^2) complexity, with each overlap comparison additionally taking O(n^2) time.  The topological sort takes linear time, O(v + e), where v is the number of vertices and e is the number of edges.

* Finally, we reconstruct the overlapping sequence.  For the sake of simplicity, I call a variation of the O(n^2) overlap function to return the overlap index. One optimization would be to cache this index when the dependency graph is initially created, allowing for reconstruction in O(m) time.

This yields a final complexity of O(m^2) * O(n^2) + O(m) + O(n^2) * O(m), which is much nicer than the exponential naive solution.

##### Potential Improvements
* More robust error handling. Right now, there is a single custom exception thrown.
* Using additional libraries instead of homegrown solutions (I/O, Fasta parsing, testing)
